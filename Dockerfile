FROM python:3.7-slim

COPY ./ ./

RUN pip install -r requirements.txt

RUN pip install wikipedia_autocomplete-0.0.1.tar.gz

EXPOSE 8050

CMD ["python", "src/app.py"]
